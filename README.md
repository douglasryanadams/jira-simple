# README #

This is currently intended to be used by a personal computer to run a simplified JIRA UI to make things like logging time and viewing issues assigned to that user easier.

JIRA has so many features that it's easy to get lost in its complex UI that provides features for doing pretty much anything. Most users (especially developers) don't care much for most of those views and settings, so this simplifies the amount of navigation and number of buttons to click.

The intent is that a user could pull this project, add their own real_config.json file with correct settings for their user, start the node server and navigate to it at localhost but have an interactive UI for JIRA.

## TODO ##

- Get total hours logged per day from JIRA directly (removing the requirement for extra API call to node server)
- complete / fixed / reopen button implementation
- Subtask creation / destruction w/o time or other stuff.