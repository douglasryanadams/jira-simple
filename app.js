/* 3rd Pary */
var express = require('express');
var app = express();
var http = require('http');
var https = require('https');
var httpProxy = require('http-proxy');
var winston = require('winston');
var connect = require('connect');

/* Custom */
var config = require('./real_config.json');
var bodyParser = connect.bodyParser();

/* Configure Logger */
var logger = new (winston.Logger)({
    "transports" : [
        new (winston.transports.Console)({
            "level" : config.log_level
        })
    ]
});

/* Configure app */
app.use(express.static('www'));

/* Business Logic */
var jiraPath = '/jira/*';
var jiraRegex  = /^\/jira\//;
var jiraForward = '/rest/api/2/';
var base64AUth = new Buffer(config.username + ":" + config.password, 'utf8').toString('base64');
var wsPath = '/jsimple/';
var timelog = {};

var WorkTimeDO = function (iWorkTimeDO) {
    this.issueKey = "";
    this.startTimeSeconds = 0;
    this.endTimeSeconds = 0;
    this.timeSeconds = 0;
    if (iWorkTimeDO) {
        this.issueKey = iWorkTimeDO.issueKey;
        this.startTimeSeconds = iWorkTimeDO.startTimeSeconds;
        this.endTimeSeconds = iWorkTimeDO.endTimeSeconds;
        this.timeSeconds = iWorkTimeDO.timeSeconds;
    }
    this.toJSON = function () {
        return {
            "issueKey" : this.issueKey
            , "startTimeSeconds" : this.startTimeSeconds
            , "endTimeSeconds" : this.endTimeSeconds
            , "timeSeconds" : this.timeSeconds
        }
    }
}

var jiraProxy = httpProxy.createProxyServer({
    "target" : "https://" + config.server ,
});

app.all(jiraPath, function (req, res) {
    logger.info('Received ', req.url);
    req.url = req.url.replace(jiraRegex, jiraForward);
    logger.debug('Proxying ', req.url);
    req.headers["Authorization"] = "Basic " + base64AUth;
    jiraProxy.web(req, res);
});

app.get(wsPath + 'default_filter', function (req, res) {
    res.send({ "filter" : config.default_filter });
});

app.put(wsPath + 'add_time/:issueKey', function (req, res) {
    bodyParser(req, res, function () {
        var issueKey = req.params.issueKey;
        logger.info("Updating timelog for issue: ", issueKey);
        var time = new WorkTimeDO(req.body);
        var timeSeconds = time.timeSeconds;
        var thisTime = new WorkTimeDO(timelog[issueKey]);
        thisTime.timeSeconds = thisTime.timeSeconds + timeSeconds;
        timelog[issueKey] = thisTime;
        res.status(200).send();
    });
});

app.put(wsPath + 'start_time/:issueKey', function (req, res) {
    var issueKey = req.params.issueKey;
    logger.info("Starting time on issue: ", issueKey);
    var thisTime = new WorkTimeDO(timelog[issueKey]);
    thisTime.startTimeSeconds = new Date().getTime() / 1000;
    timelog[issueKey] = thisTime;
    logger.debug(thisTime);
    res.status(200).send();
});

app.put(wsPath + 'end_time/:issueKey', function (req, res) {
    var issueKey = req.params.issueKey;
    logger.info("Ending time on issue: ", issueKey);
    var thisTime = new WorkTimeDO(timelog[issueKey]);
    thisTime.endTimeSeconds = new Date().getTime() / 1000;
    logger.debug(thisTime);
    if ( thisTime.startTimeSeconds != 0
        && (thisTime.endTimeSeconds - thisTime.startTimeSeconds) > 60) {
        res.send(thisTime.toJSON());
        timelog[issueKey]['startTimeSeconds'] = 0;
    } else {
        res.status(400).send({ "errorMessages" : ["Time never started for this issue, must end at least one minute after starting."]});
    }
});

app.get(wsPath + 'issue_time/:issueKey', function (req, res) {
    var issueKey = req.params.issueKey;
    logger.info("Retrieving time information about: ", issueKey);
    var thisTime = new WorkTimeDO(timelog[issueKey]);
    res.send(thisTime.toJSON());
});

/* Start Server */
logger.info('Starting jira-simple');
app.listen(3000);
