jsimple.controller('HomeController', function (
    $scope
    , $http
    , $log
    , UserDO
    , FilterDO
    , IssueDO
    , SearchResultDO
    , WorklogDO
    , WorklogTimeDO
    , TimeDO
    ) {

    var jiraURL = '/jira/';
    var wsURL = '/jsimple/';
    var filterLookup = {};

    $scope.filters = [];
    $scope.issues = [];
    $scope.errors = [];
    $scope.started = false;

    var handleError = function (data) {
        $log.error(data);
        $scope.errors = $scope.errors.concat(data.errorMessages);
        for (error in data.errors) {
            $scope.errors.push(data.errors[error]);
        }
        $log.error($scope.errors);
    };

    var setDefaultFilter = function () {
        $http.get(wsURL + 'default_filter')
            .success(function (data) {
                var filter = data.filter;
                $scope.filter = filterLookup[filter];
            })
            .error(handleError);
    };

    var loadFavoriteFilters = function () {
        $http.get(jiraURL + 'filter/favourite')
            .success(function (data) {
                $scope.filters = [];
                filterLookup = {};
                var filters = data;
                for (var i = 0; i < filters.length; i++) {
                    var filter = new FilterDO(filters[i]);
                    $scope.filters[i] = filter;
                    filterLookup[filter.name] = $scope.filters[i];
                };
                $scope.errors = [];
                setDefaultFilter();
            })
            .error(handleError);
    };

    var loadIssues = function (filter) {
        var requestString = jiraURL + "search?jql=" + filter.jql;
        $log.info("Requesting: " , requestString);
        $http.get(requestString)
            .success(function (data) {
                var result = new SearchResultDO(data);
                $log.info("Retrieved data: ", result);
                $scope.issues = result.issues;
                $scope.errors = [];
            })
            .error(handleError);
    };

    var postTime = function (seconds) {
        var worklog = new WorklogDO();
        worklog.timeSpentSeconds = parseInt(seconds);
        worklog.comment = "Added via jira-simple.";
        var now = new Date().getTime() / 1000;
        worklog.started = new TimeDO(now - worklog.timeSpentSeconds);
        var url = jiraURL + "issue/" + $scope.issue.key + "/worklog"; // TODO
        $log.debug("Worklog: ", worklog);
        $log.debug("Worklog JSON: ", worklog.toJSON());
        $http.post(url, worklog.toJSON())
            .success(function (data) {
                $log.info("Successful time addition!");
                var addTime = new WorklogTimeDO();
                addTime.timeSeconds = worklog.timeSpentSeconds;
                $http.put(wsURL + 'add_time/' + $scope.issue.key, addTime)
                    .success(function(data) {
                        $log.info("Successfully updated server.");
                        $scope.errors = [];
                    })
                    .error(handleError);
            })
            .error(handleError);
    }

    /**
    * 1 = Open
    * 3 = In Progress
    * 5 = Resolved
    * 10003 = Completed
    */
    $scope.display = function (listIds) {
        if ($scope.issue) {
            var idInt = parseInt($scope.issue.fields.status.id);
            if (listIds.indexOf(idInt) >= 0 ) {
                return true;
            }
        }
        return false;
    };

    $scope.selectIssue = function (issue) {
        $scope.issue = issue;
        $http.get(wsURL + "issue_time/" + $scope.issue.key)
            .success(function (data) {
                var time = new WorklogTimeDO(data);
                if (time.startTimeSeconds > 0) {
                    $scope.started = true;
                } else {
                    $scope.started = false;
                }
            })
            .error(handleError);
    };

    $scope.addTime = function () {
        $log.info("Adding time: ", $scope.loggedHours);
        postTime( parseFloat($scope.loggedHours) * 3600 );
    };

    $scope.startTime = function () {
        $log.info("Starting Time");
        $http.put(wsURL + "start_time/" + $scope.issue.key)
            .success(function (data) {
                $log.info("Time started");
                $scope.started = true;
            })
            .error(handleError);
    };

    $scope.endTime = function () {
        $log.info("Ending Time");
        $http.put(wsURL + "end_time/" + $scope.issue.key)
            .success(function (data) {
                var time = new WorklogTimeDO(data);
                postTime(time.endTimeSeconds - time.startTimeSeconds);
                $scope.started = false;
            })
            .error(handleError);
    };

    $scope.completeIssue = function () {
        $log.info("Completing issue: ", $scope.issue.key);
    };

    $scope.fixIssue = function () {
        $log.info("Fixing issue: ", $scope.issue.key);
    };

    $scope.reopenIssue = function () {
        $log.info("Reopening issue: ", $scope.issue.key);
    }

    $scope.$watch('filter', function () {
        var filter = $scope.filter;
        if (filter) {
            loadIssues(filter);
        }
    });

    var main = function () {
        loadFavoriteFilters();
        $log.info('Loaded home');
    };



    main();
});