var jsimple = angular.module('jsimple', [
    'ngRoute'
]);

jsimple.config([
    '$routeProvider',
    function ($routeProvider) {
        $routeProvider
            .when('/', {
                "templateUrl" : "home.html"
                , "controller" : "HomeController"
            })
            .otherwise({
                "redirectTo" : "/"
            });
    }
]);