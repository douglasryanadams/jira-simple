jsimple.service('WorklogTimeDO', function (
        FieldDO
    ) {
    var WorklogTimeDO = function (iWorklogTime) {
        this.issueKey = "";
        this.startTimeSeconds = 0;
        this.endTimeSeconds = 0;
        this.timeSeconds = 0;
        if (iWorklogTime) {
            this.issueKey = iWorklogTime.issueKey;
            this.startTimeSeconds = iWorklogTime.startTimeSeconds;
            this.endTimeSeconds = iWorklogTime.endTimeSeconds;
            this.timeSeconds = iWorklogTime.timeSeconds;
        }
        var toJSON = function () {
            return {
                "issueKey" : this.issueKey
                , "startTimeSeconds" : this.startTimeSeconds
                , "endTimeSeconds" : this.endTimeSeconds
                , "timeSeconds" : this.timeSeconds
            }
        }
    };

    return WorklogTimeDO;
});