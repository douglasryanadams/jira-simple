jsimple.service('IssueDO', function (
        FieldDO
    ) {
    var IssueDO = function (iIssue) {
        this.self = "";
        this.key = "";
        this.id = "";
        this.fields = new FieldDO();

        if (iIssue) {
            this.self = iIssue.self;
            this.key = iIssue.key;
            this.id = iIssue.id;
            this.fields = new FieldDO(iIssue.fields);
        }
    };

    return IssueDO;
});