jsimple.service('StatusDO', function (
    ) {
    var StatusDO = function (iStatus) {
        this.self = "";
        this.name = "";
        this.id = "";
        this.description = "";

        if (iStatus) {
            this.self = iStatus.self;
            this.name = iStatus.name;
            this.id = iStatus.id;
            this.description = iStatus.description;
        }
    };

    return StatusDO;
});