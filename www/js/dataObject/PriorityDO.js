jsimple.service('PriorityDO', function (
    ) {
    var PriorityDO = function (iPriority) {
        this.self = "";
        this.name = "";
        this.id = "";

        if (iPriority) {
            this.self = iPriority.self;
            this.name = iPriority.name;
            this.id = iPriority.id;
        }
    };

    return PriorityDO;
});