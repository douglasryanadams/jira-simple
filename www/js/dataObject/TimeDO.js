jsimple.service('TimeDO', function (
    $log
    ) {
    var TimeDO = function (iTime) {
        var self = this;
        self.year = 0;
        self.month = 0;
        self.day = 0;
        self.hours = 0;
        self.minutes = 0;
        self.seconds = 0;
        self.timeAsString = "";

        var setTimeString = function () {
            self.timeAsString = [self.year, '-', self.month, '-', self.day, 'T', self.hours, ':', self.minutes, ":", self.seconds, ".000+0000" ].join('');
        };

        var constructFromString = function (time) {
            // Expecting something like: 2014-05-22T16:51:02.000+0000
            var pieces = time.match(/\d+/g);
            for (var i = 0; i < pieces.length; i++) {
                pieces[i] = parseInt(pieces[i]);
            }
            self.year = pieces[0];
            self.month = pieces[1];
            self.day = pieces[2];
            self.hours = pieces[3];
            self.minutes = pieces[4];
            self.seconds = pieces[5];
            setTimeString();
        };

        if (iTime) {
            if (typeof iTime === 'string') {
                $log.debug("Constructing Time from string: ", iTime);
                constructFromString(iTime);
            }
            else if (typeof iTime === 'number') {
                $log.debug("Constructing time from number: ", iTime);
                var date = new Date(iTime * 1000);
                constructFromString(date.toISOString());
            }
            else {
                $log.debug("Constructing time from object: ", iTime);
                self.year = iTime.year;
                self.month = iTime.month;
                self.day = iTime.day;
                self.hours = iTime.hours;
                self.minutes = iTime.minutes;
                self.seconds = iTime.seconds;
                setTimeString();
            }
        }
    };

    return TimeDO;
});