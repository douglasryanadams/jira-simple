jsimple.service('IssueTypeDO', function (
    ) {
    var IssueTypeDO = function (iIssueType) {
        this.self = "";
        this.id = "";
        this.description = "";
        this.name = "";
        if (iIssueType) {
            this.self = iIssueType.self;
            this.id = iIssueType.id;
            this.description = iIssueType.description;
            this.name = iIssueType.name;
        }
    };
    return IssueTypeDO;
});