jsimple.service('WorklogDO', function (
        UserDO
        , TimeDO
    ) {
    var WorklogDO = function (iWorklog) {
        this.author = new UserDO();
        this.comment = "";
        this.timeSpentSeconds = 0;
        this.started = new TimeDO();
        if (iWorklog) {
            this.author = iWorklog.author;
            this.comment = iWorklog.comment;
            this.timeSpentSeconds = iWorklog.timeSpentSeconds;
            this.started = new TimeDO(iWorklog.started);
        }

        this.toJSON = function () {
            return {
                "author" : this.author
                , "comment" : this.comment
                , "timeSpentSeconds" : this.timeSpentSeconds
                , "started" : this.started.timeAsString
             }
        }
    };

    return WorklogDO;
});