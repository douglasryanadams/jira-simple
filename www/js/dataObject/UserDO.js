jsimple.service('UserDO', function (
    ) {
    var UserDO = function (iUser) {
        this.self = "";
        this.emailAddress = "";
        this.name = "";
        this.displayName = "";

        if (iUser) {
            this.self = iUser.self;
            this.emailAddress = iUser.emailAddress;
            this.name = iUser.name;
            this.displayName = iUser.displayName;
        }
    };

    return UserDO;
});