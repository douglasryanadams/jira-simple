jsimple.service('SearchResultDO', function (
        IssueDO
    ) {
    var SearchResultDO = function (iSearchResult) {
        this.startAt = 0;
        this.maxResults = 0;
        this.total = 0;
        this.issues = []; // IssueDO

        if (iSearchResult) {
            this.startAt = iSearchResult.startAt;
            this.maxResults = iSearchResult.maxResults;
            this.total = iSearchResult.total;
            var issueList = iSearchResult.issues;
            for (var i = 0; i < issueList.length; i++) {
                this.issues.push(new IssueDO(issueList[i]));
            }
        }
    };

    return SearchResultDO;
});