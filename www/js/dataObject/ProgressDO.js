jsimple.service('ProgressDO', function (
    ) {
    var ProgressDO = function (iProgress) {
        this.progress = 0;
        this.total = 0;
        this.percent = 0;

        if (iProgress) {
            this.progress = iProgress.progress;
            this.total = iProgress.total;
            this.percent = iProgress.percent;
        }
    };

    return ProgressDO;
});