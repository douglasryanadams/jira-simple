jsimple.service('FieldDO', function (
        UserDO
        , ProgressDO
        , IssueTypeDO
        , TimeDO
        , PriorityDO
        , StatusDO
        , ProjectDO
        , FixVersionDO
        , $sce
    ) {
    var FieldDO = function (iField) {
        this.summary = "";
        this.description = "";
        this.progress = new ProgressDO();
        this.issuetype = new IssueTypeDO();
        this.reporter = new UserDO();
        this.created = new TimeDO();
        this.updated = new TimeDO();
        this.lastViewed = new TimeDO();
        this.priority = new PriorityDO();
        this.status = new StatusDO();
        this.project = new ProjectDO();
        this.fixVersions = []; // FixVersionDO
        this.assignee = new UserDO();

        var fixDescription = function (description) {
            if (description) {
                description = description.replace(/\r\n/g,'<br>');
            } else {
                description = "";
            }
            $sce.trustAsHtml(description);
            return description;
        }

        if (iField) {
            this.summary = iField.summary;
            if (iField.description) {
                this.description = $sce.trustAsHtml(iField.description.replace(/\r\n/g,'<br>'));
            }
            this.progress = new ProgressDO(iField.progress);
            this.issuetype = new IssueTypeDO(iField.issuetype);
            this.reporter = new UserDO(iField.reporter);
            this.created = new TimeDO(iField.created);
            this.updated = new TimeDO(iField.updated);
            this.lastViewed = new TimeDO(iField.lastViewed);
            this.priority = new PriorityDO(iField.priority);
            this.status = new StatusDO(iField.status);
            this.project = new ProjectDO(iField.project);
            for (var i = 0; i < iField.fixVersions.length; i++) {
                this.fixVersions.push(new FixVersionDO(iField.fixVersions[i]));
            }
            this.assignee = new UserDO(iField.assignee);
        }
    };

    return FieldDO;
});