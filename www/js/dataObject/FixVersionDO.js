jsimple.service('FixVersionDO', function (
        TimeDO
    ) {
    var FixVersionDO = function (iFixVersion) {
        this.self = "";
        this.id = "";
        this.name = "";
        this.description = "";
        this.releaseDate = new TimeDO();

        if (iFixVersion) {
            this.self = iFixVersion.self;
            this.id = iFixVersion.id;
            this.name = iFixVersion.name;
            this.description = iFixVersion.description;
            this.releaseDate = new TimeDO(iFixVersion.releaseDate);
        }
    };

    return FixVersionDO;
});