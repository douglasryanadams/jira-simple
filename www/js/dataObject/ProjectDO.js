jsimple.service('ProjectDO', function (
    ) {
    var ProjectDO = function (iProject) {
        this.self = "";
        this.key = "";
        this.id = "";
        this.name = "";

        if (iProject) {
            this.self = iProject.self;
            this.key = iProject.key;
            this.id = iProject.id;
            this.name = iProject.name;
        }
    };

    return ProjectDO;
});