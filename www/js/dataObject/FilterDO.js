jsimple.service('FilterDO', function (
    UserDO
    ) {

    var FilterDO = function (iFilter) {
        this.self = "";
        this.id = "";
        this.name = "";
        this.owner = new UserDO();
        this.jql = "";
        this.searchUrl = "";
        if (iFilter) {
            this.self = iFilter.self;
            this.id = iFilter.id;
            this.name = iFilter.name;
            this.owner = new UserDO(iFilter.owner);
            this.jql = iFilter.jql;
            this.searchUrl = iFilter.searchUrl;
        }
    };

    return FilterDO;
});